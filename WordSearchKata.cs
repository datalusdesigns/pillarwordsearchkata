using System.IO;
using System;
namespace PillarWordSearchKata {

    class WordSearchKata {
        
        public FileInfo[] getFiles(string filename = null) {
            DirectoryInfo dir = new DirectoryInfo(Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar + Constants.GRIDS_DIR + Path.DirectorySeparatorChar);
            FileInfo[] files = (filename == null) ? dir.GetFiles("*.txt") : dir.GetFiles(filename);
            return files;
        }



        public void ProcessGridAndOutputResults() {
            FileInfo[] files = getFiles();
            foreach (FileInfo file in files) {
                WordSearch ws = new WordSearch(file);

                string results = null;

                foreach (string word in ws.Words) {
                    foreach (Func<string, Point[]> func in new Func<string, Point[]>[] { 
                        ws.findWord_Horizontally,
                        ws.findWord_Vertically,
                        ws.findWord_DiagonallyDescending,
                        ws.findWord_DiagonallyAscending,
                        ws.findWord_Backwards,
                     }) {

                        Point[] points = func(word);
                        if (points != null && points.Length > 0) {
                            results += string.Format("{0}: {1}{2}", word, string.Join<Point>(',', points), Environment.NewLine);
                            break;  //Prevents the same word that matches in two or more directions from appearing more than once
                        }

                    }
                }


                Console.WriteLine(string.Format("File: {0}", ws.FileName));
                Console.WriteLine(results ?? "No words found");
                Console.WriteLine();
                
            }
        }
    }

}