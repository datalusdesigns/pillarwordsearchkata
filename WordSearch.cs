using System.IO;
using System.Linq;
using System;
using System.Collections.Generic;

namespace PillarWordSearchKata {

    interface WordSearchInterface {
        //User stories require testing these specifically so our object has to have these instead of a unified method of enumerated directions
        Point[] findWord_Horizontally(string word);                     //from left-to-right
        Point[] findWord_Vertically(string word);                       //from top-to-bottom
        Point[] findWord_DiagonallyDescending(string word);             //from top-left to bottom-right
        Point[] findWord_DiagonallyAscending(string word);              //from bottom-left to top-right
        Point[] findWord_Backwards(string word);                        //from right-to-left
    }

    public class WordSearch : WordSearchInterface {

        private FileInfo _file;
        private string _filename;
        private int _grid_size;      //Always square so this is height and width
        private string[] _words;
        private string[][] _grid;

        public WordSearch(FileInfo file) {
            _file = file;

            string[] lines = File.ReadAllText(file.FullName).Split(System.Environment.NewLine);

            _words = lines[0].Split(Constants.GRIDFILE_SEPARATOR);     //Array of the words on the first line
            string[] _gridString = lines.Skip(1).ToArray();            //Pop the first line off. It's in words now
            _grid_size = _gridString[0].Split(Constants.GRIDFILE_SEPARATOR).Length;

            _grid = new string[_grid_size][];
            for (int i = 0; i < _grid_size; i++)
                _grid[i] = _gridString[i].Split(Constants.GRIDFILE_SEPARATOR);

            _filename = _file.Name;
        }

        public string FileName { get { return _filename; } }
        public string[] Words { get { return _words; } }
        public string[][] Grid { get { return _grid; } }
        public int Size { get { return _grid_size; } }

        // public Point[] FindWordByDirection(Func<string, Point[]> fxFindWordByDirection, string word) {
        //     return fxFindWordByDirection(word);
        //     //return (points.Length > 0) ? String.Format("{0}: {1}{2}", word, string.Join<Point>(',', points), Environment.NewLine) : String.Empty;
        // }

        public Point[] FindWord(int direction, string word) {
            //Get all possible starting positions
            string firstLetter = word[0].ToString();
            for (int x = 0; x < this._grid_size; x++)
                for (int y = 0; y < this._grid_size; y++)
                    if (this._grid[x][y] == firstLetter) {        //Find starting position
                        List<Point> matching = new List<Point>() { new Point(x,y) };
                        string t_word = word;   //Temp word to traverse letters
                        int test = 0;

                        while (true) {
                            if (++test > _grid_size + 1) throw new Exception(string.Format("while(true) has a hole. t_word={0} dir={1} ", t_word, direction.ToString() ));

                            t_word = t_word.Substring(1);   //Pop found letter
                            Point next = new Point(matching[matching.Count - 1].x + Constants.DIRECTIONS[direction].x,
                                                   matching[matching.Count - 1].y + Constants.DIRECTIONS[direction].y );
                            
                            //Gauntlet
                            if (t_word == String.Empty) return matching.ToArray();                                        //All letters found
                            if (next.x >= _grid_size || next.x < 0 || next.y >= _grid_size || next.y < 0) break;            //Next position is out of grid
                            if (this._grid[next.x][next.y] != t_word[0].ToString()) break;                                //Next letter doesn't match

                            matching.Add( new Point(next.x, next.y) );
                        }
                    }
            return null;
        }

        public Point[] findWord_Horizontally(string word) {                     //from left-to-right
            return FindWord(Constants.DIRECTION_HORIZONTALLY, word);
        }

        public Point[] findWord_Vertically(string word) {                       //from top-to-bottom
            return FindWord(Constants.DIRECTION_VERTICALLY, word);
        }

        public Point[] findWord_DiagonallyDescending(string word) {             //from top-left to bottom-right
            return FindWord(Constants.DIRECTION_DIAGONALLY_DESCENDING, word);
        }

        public Point[] findWord_DiagonallyAscending(string word) {              //from bottom-left to top-right
            return FindWord(Constants.DIRECTION_DIAGONALLY_ASCENDING, word);
        }

        public Point[] findWord_Backwards(string word) {                        //from right-to-left
            return FindWord(Constants.DIRECTION_BACKWARDS, word);
        }
                

    }

}