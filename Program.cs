﻿using System;

namespace PillarWordSearchKata
{
    class Program
    {
        static void Main(string[] args)
        {
            PillarWordSearchKata.WordSearchKata wordSearchKata = new WordSearchKata();
            wordSearchKata.ProcessGridAndOutputResults();
        }
    }
}
