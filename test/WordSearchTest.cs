namespace PillarWordSearchKata.Tests {

    using NUnit.Framework;
    using System.IO;
    using System.Collections;
    using System.Linq;
    using System;

    [TestFixture]
    class WordSearchKataTests {

        WordSearchKata wordSearchKata;

        [SetUp] public void Init() {
            wordSearchKata = new WordSearchKata();
        }

        //[TestCase("grid.txt")]      //Uncomment to test specific file. Otherwise, it will test all .txt files in the 'grids' folder
        [TestCase]
        public void WordSearchProcessedProperlyAndFileHasProperFormat(string filename = null) {
            //Testing the following conditions:
            //1. Contains at least one line
            //2. No characters except uppercase letters in words
            //3. All grid characters are single letters
            //4. First line contains at least one search word
            //5. All search words are at least two letters long
            //6. The grid is a square

            /****
            WordSearch object should have the following properties:
            string[<grid size>][<grid size>] Grid
            string[] Words
            ****/

            FileInfo[] files = wordSearchKata.getFiles(filename);

            foreach (FileInfo file in files)
            {
                Console.WriteLine(file.ToString());
                WordSearch ws = new WordSearch(file);
                
                //1. Contains at least one line
                Assert.IsTrue( ws.Words.Length > 0, "File appears to be empty" );
                
                //2. No characters except uppercase letters in words
                foreach (string word in ws.Words)
                    Assert.That( word, Does.Match( "^[A-Z]*$" ), "Invalid character found in words line" );

                //3. All grid characters are single letters
                foreach (string[] line in ws.Grid) 
                    foreach (string letter in line)    
                        Assert.That( letter, Does.Match( "^[A-Z]$" ), "Invalid character found in grid. Must be an uppercase, single letter" );

                //4. First line contains at least one search word
                Assert.IsTrue( ws.Words.Length > 0, "Requires at least one word" );

                //5. All search words are at least two letters long
                foreach (string word in ws.Words)
                    Assert.IsTrue( word.Length > 1, "Words must be at least two letters long" );

                //6. The grid is a square
                int x = ws.Grid[0].Length;
                int y = ws.Grid.Length;
                Assert.IsTrue( x == y, "Grid is not a square" );

            }

        }

        private void WordCanBeFound(string filename, string word, int direction) {
            FileInfo file = wordSearchKata.getFiles(filename)[ 0 ];
            WordSearch ws = new WordSearch(file);
            
            Func<string, Point[]> fxFindWordByDirection;
            if (direction == Constants.DIRECTION_HORIZONTALLY) fxFindWordByDirection = ws.findWord_Horizontally;
            else if (direction == Constants.DIRECTION_VERTICALLY) fxFindWordByDirection = ws.findWord_Vertically;
            else if (direction == Constants.DIRECTION_DIAGONALLY_DESCENDING) fxFindWordByDirection = ws.findWord_DiagonallyDescending;
            else if (direction == Constants.DIRECTION_DIAGONALLY_ASCENDING) fxFindWordByDirection = ws.findWord_DiagonallyAscending;
            else if (direction == Constants.DIRECTION_BACKWARDS) fxFindWordByDirection = ws.findWord_Backwards;
            else throw new Exception("Invalid direction provided: " + direction.ToString());
            Point[] points = fxFindWordByDirection( word );

            Assert.IsTrue( points != null && points.Length > 0, string.Format("Word [{0}] was not found in '{1}'.", word, filename) );
        }

        //Invalid
        [TestCase("original.txt", "SPOCK")]
        [TestCase("hotel.txt", "SOIL")]
        [TestCase("HIHO.txt", "HI")]
        [TestCase("original_moved_SCOTTY.txt", "SCOTTY")]
        //Valid
        [TestCase("hotel.txt", "HOTEL")]
        [TestCase("hotel.txt", "PULL")]
        [TestCase("OK.txt", "OK")]
        [TestCase("HIHO.txt", "HO")]
        [TestCase("original.txt", "SCOTTY")]
        public void WordCanBeFound_Horizontally(string filename, string word) {
            WordCanBeFound(filename, word, Constants.DIRECTION_HORIZONTALLY);
        }

        //Invalid
        [TestCase("original.txt", "SPOCK")]
        [TestCase("OK.txt", "OK")]
        [TestCase("HIHO.txt", "HO")]
        //Valid
        [TestCase("HIHO.txt", "HI")]

        public void WordCanBeFound_Vertically(string filename, string word) {
            WordCanBeFound(filename, word, Constants.DIRECTION_VERTICALLY);
        }

        //Invalid
        [TestCase("original.txt", "SCOTTY")]
        //Valid
        [TestCase("original_moved_SCOTTY.txt", "SCOTTY")]
        [TestCase("original.txt", "SPOCK")]
        public void WordCanBeFound_DiagonallyDescending(string filename, string word) {
            WordCanBeFound(filename, word, Constants.DIRECTION_DIAGONALLY_DESCENDING);
        }

        //Invalid
        [TestCase("original.txt", "SPOCK")]
        //Valid
        [TestCase("HIHO.txt", "OH")]
        public void WordCanBeFound_DiagonallyAscending(string filename, string word) {
            WordCanBeFound(filename, word, Constants.DIRECTION_DIAGONALLY_ASCENDING);
        }

        //Valid
        [TestCase("HIHO.txt", "OH")]
        [TestCase("original.txt", "SPOCK")]
        //Invalid
        [TestCase("HIHO.txt", "HO")]
        public void WordCanBeFound_Backwards(string filename, string word) {
            WordCanBeFound(filename, word, Constants.DIRECTION_BACKWARDS);
        }

    }
}