# PillarWordSearchKata

Name: Pillar Word Search Kata
Date: July 6th, 2019
Author: Kol David Gudlaugsson

Project Description: Take in a list of words and a grid from a text file and list out the positions of the letters of matched words.

Project Stack: 
    Written in C# with NUnit TDD.
    Built with .NET core v2.2 (dotnet).
    Developed in VSCode.
    Repo on BitBucket.

Repo: https://datalusdesigns@bitbucket.org/datalusdesigns/pillarwordsearchkata.git

Requires: dotnet. 
Dependencies:
    Dependencies pulled via NuGet and should automatically download. 
    <PackageReference Include="Microsoft.NET.Test.Sdk" Version="16.1.1" />
    <PackageReference Include="NUnit" Version="3.12.0" />
    <PackageReference Include="Nunit3TestAdapter" Version="3.13.0" />

Commands:
    dotnet build  (or within VSCode. VS should run fine, too)
    dotnet test   (or within VSCode. VS should run fine, too)
    dotnet run    (or within VSCode. VS should run fine, too)
    'grids' asset folder is copied to the build folder automatically for any of the commands

Post-mortem: The actual project itself was very simple. A few irritations with file manipulation and some interesting grid traversal but - for the most part - pretty straight-forward. But I was surprised to find that the test designing was more difficult than I expected simply because the user stories requested that the differing directions be tested individually. It forced 5 awkard methods to exist in the tests and main WordSearch object that I would have avoided. Fortunately, delegates removed most of the redundancy, but I still feel like the directions are relisted more often than maybe necessary. I would have tried to steer the product owner away from requiring testing on a per-direction-basis since it's mostly a technical distinction anyways.

User Stories: 
    As the Puzzle Solver
    I want to search horizontally
    So that I can find words on the X axis

    As the Puzzle Solver
    I want to search vertically
    So that I can find words on the Y axis

    As the Puzzle Solver
    I want to search diagonally descending
    So that I can find words the descend along the X axis

    As the Puzzle Solver
    I want to search diagonally ascending
    So that I can find words that ascend along the X axis

    As the Puzzle Solver
    I want to search backwards
    So that I can find words in reverse along all axes