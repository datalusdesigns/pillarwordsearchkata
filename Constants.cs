using System.Collections.Generic;
namespace PillarWordSearchKata {
    public static class Constants
    {
        public const string GRIDS_DIR = "grids";
        public const string GRIDFILE_SEPARATOR = ",";

        public const int DIRECTION_HORIZONTALLY = 0;
        public const int DIRECTION_VERTICALLY = 1;
        public const int DIRECTION_DIAGONALLY_DESCENDING = 2;
        public const int DIRECTION_DIAGONALLY_ASCENDING = 3;
        public const int DIRECTION_BACKWARDS = 4;
        public static Dictionary<int, Point> DIRECTIONS = new Dictionary<int, Point>() {
            { DIRECTION_HORIZONTALLY,           new Point( 1,  0) },
            { DIRECTION_VERTICALLY,             new Point( 0,  1) },
            { DIRECTION_DIAGONALLY_DESCENDING,  new Point( 1,  1) },
            { DIRECTION_DIAGONALLY_ASCENDING,   new Point(-1, -1) },
            { DIRECTION_BACKWARDS,              new Point(-1,  0) },
        };
    }
}