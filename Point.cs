using System;
public class Point {
    public int x;
    public int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public override string ToString() {
        return String.Format("({0},{1})", this.x.ToString(), this.y.ToString());        //TODO move format to constants
    }
}